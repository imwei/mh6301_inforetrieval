package yelp;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.DoublePoint;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.Query;

import java.util.Arrays;
import java.util.List;

/**
 * Created by wangw on 03-Apr-17.
 */
public class YelpCustomQueryParser extends QueryParser {
    private static final List<String> FIELDS_DOUBLE = Arrays.asList("stars");
    private static final List<String> FIELDS_LONG = Arrays.asList("review_count", "useful", "funny", "cool");

    public YelpCustomQueryParser(String f, Analyzer a) {
        super(f, a);
    }

    @Override
    protected Query getRangeQuery(String field, String low, String high, boolean startInclusive, boolean endInclusive) throws ParseException {
        if (FIELDS_DOUBLE.contains(field.toLowerCase())){
            return DoublePoint.newRangeQuery(field, Double.valueOf(low), Double.valueOf(high));
        }else if (FIELDS_LONG.contains(field.toLowerCase())){
            return LongPoint.newRangeQuery(field, Long.valueOf(low), Long.valueOf(high));
        }
        return super.getRangeQuery(field, low, high, startInclusive, endInclusive);
    }
}
