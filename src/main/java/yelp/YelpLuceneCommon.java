package yelp;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by wangw on 03-Apr-17.
 */
public abstract class YelpLuceneCommon {

    public void index(String yelpDataDir, IndexWriter indexWriter) throws IOException {
        Files.lines(Paths.get(yelpDataDir, getFileNameToIndex())).forEach(line->{
            Object fileObjects= JSONValue.parse(line);
            try {
                indexWriter.addDocument(createDoc((JSONObject)fileObjects));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public abstract String getFileNameToIndex();

    public abstract Document createDoc(JSONObject jsonData);

    public abstract List<Query> getQueries(Analyzer analyzer) throws ParseException;

    public void indexAndSearch(String yelpDataDir, Similarity similarity) throws IOException, ParseException {
        // 0. Specify the analyzer for tokenizing text.
        //    The same analyzer should be used for indexing and searching
        StandardAnalyzer analyzer = new StandardAnalyzer();

        // 1. create the index
        Directory index = new RAMDirectory();

        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        if (similarity!=null){
            config.setSimilarity(similarity);
        }
        System.out.println("Using similarity:"+config.getSimilarity());
        IndexWriter w = new IndexWriter(index, config);
        index(yelpDataDir, w);
        w.close();

        //prepare for search
        int hitsPerPage = 10;
        IndexReader reader = DirectoryReader.open(index);

        // 2. query
        for(Query query:getQueries(analyzer)){
            System.out.println("working on query:"+query);
            // 3. search
            IndexSearcher searcher = new IndexSearcher(reader);
            if (similarity!=null){
                searcher.setSimilarity(similarity);
            }

            TopScoreDocCollector collector = TopScoreDocCollector.create(hitsPerPage);
            searcher.search(query, collector);

            ScoreDoc[] hits = collector.topDocs().scoreDocs;

            // 4. display results
            System.out.println("Found " + hits.length + " hits.");
            List<Document> results = new LinkedList<>();
            for(int i=0;i<hits.length;++i) {
                results.add(searcher.doc(hits[i].doc));
            }
            printSearchResult(results);
        }

        // reader can only be closed when there
        // is no need to access the documents any more.
        reader.close();
        System.out.println("total ram:"+Runtime.getRuntime().totalMemory());
        System.out.println("free ram:"+Runtime.getRuntime().freeMemory());
    }

    public abstract void printSearchResult(List<Document> results);
}
