package yelp;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.ClassicSimilarity;
import org.apache.lucene.search.similarities.LMDirichletSimilarity;
import org.apache.lucene.spatial.geopoint.document.GeoPointField;
import org.apache.lucene.spatial.geopoint.search.GeoPointDistanceQuery;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class YelpLuceneBusinessRAM extends YelpLuceneCommon {
    private static final String DEFAULT_SEARCH_FIELD = "name";
    private static final String[] QUERIES = {
            "name:pizza",
            "+city:\"las vegas\" +categories:\"fast food\"",
            "+city:\"vegas\" +categories:\"restaurant\"",
            "review_count:[1000 TO 1000000]",
            "+categories:restaurants +stars:[5.0 TO 5.0]",
            "+categories:restaurants +stars:[4.0 TO 5.0]",
            "city:\"las vegas\" -categories:restaurants",
            "+city:\"las vegas\" +categories:restaurants +categories:thai",
            "city:\"las vegas\" city:edinburgh",
            "+(+categories:restaurants) +(categories:thai categories:chinese)",
            "+name:'gordon ramsay'",
            "+name:plumb* +city:vegas",
            "+city:edinburgh +name:pharma*",
            "+name:'authentic italian pizza pasta'~1",
            "+name:'fish chips'~2"
    };

    @Override
    public List<Query> getQueries(Analyzer analyzer) throws ParseException {
        QueryParser queryParser = new YelpCustomQueryParser(DEFAULT_SEARCH_FIELD, analyzer);
        List<Query> result = new LinkedList<>();
        for(String queryText:QUERIES){
            result.add(queryParser.parse(queryText));
        }
        //below geopoint query is not supported by query parser, thus commented out, replace with coded queries
        //"+categories:restaurants +stars:[4.0 TO 5.0] +GeoPointDistanceQuery: field=geolocation: Center: [36.0902894,-115.174297] Distance: 2000.0 meters]"
        TermQuery fourandabove_restaurant_near_fourseasons_lasvegas_t1 = new TermQuery(new Term("categories", "Restaurants".toLowerCase()));
        Query fourandabove_restaurant_near_fourseasons_lasvegas_t2 = DoublePoint.newRangeQuery("stars",4.0, 5.0);
        //within 2km radius near Four Seasons Las Vegas
        Query fourandabove_restaurant_near_fourseasons_lasvegas_t3 = new GeoPointDistanceQuery("geolocation", 36.0902894, -115.174297,  2000);
        BooleanQuery fourandabove_restaurant_near_fourseasons_lasvegas = new BooleanQuery.Builder()
                .add(fourandabove_restaurant_near_fourseasons_lasvegas_t1, BooleanClause.Occur.MUST)
                .add(fourandabove_restaurant_near_fourseasons_lasvegas_t2, BooleanClause.Occur.MUST)
                .add(fourandabove_restaurant_near_fourseasons_lasvegas_t3, BooleanClause.Occur.MUST)
                .build();
        result.add(fourandabove_restaurant_near_fourseasons_lasvegas);
        return result;
    }

    @Override
    public void printSearchResult(List<Document> results) {
        // 4. display results
        for(int i=0;i<results.size();++i) {
            Document d = results.get(i);
            System.out.println((i + 1) + ".\t" + d.get("name") + " | " + d.get("address") + " | " + d.get("city") +" | " + d.get("stars") + "\r\n"
                    + "\t" + Arrays.toString(d.getValues("categories")));
        }
    }

    @Override
    public String getFileNameToIndex() {
        return "yelp_academic_dataset_business.json";
    }

    /**
     * Add documents to the index
     */
    @Override
    public Document createDoc(JSONObject jsonBiz){
        Document doc = new Document();
        for(String fieldName : (Set<String>) jsonBiz.keySet()){
            Object fieldValue = jsonBiz.get(fieldName);
            switch(fieldName){
                case "business_id":
                    doc.add(new StringField(fieldName, fieldValue.toString(), Field.Store.YES));
                    break;
                case "name":
                    doc.add(new TextField(fieldName, fieldValue.toString(), Field.Store.YES));
                    break;
                case "address":
                    doc.add(new TextField(fieldName, fieldValue.toString(), Field.Store.YES));
                    break;
                case "city":
                    doc.add(new TextField(fieldName, fieldValue.toString(), Field.Store.YES));
                    break;
                case "state":
                    doc.add(new StringField(fieldName, fieldValue.toString(), Field.Store.YES));
                    break;
                case "review_count":
                    doc.add(new LongPoint(fieldName, (Long)fieldValue));
                    doc.add(new StoredField(fieldName, (Long)fieldValue));
                    break;
                case "stars":
                    doc.add(new DoublePoint(fieldName, Double.valueOf(fieldValue.toString())));
                    doc.add(new StoredField(fieldName, Double.valueOf(fieldValue.toString())));
                    break;
            }
        }

        // add categories
        Object attributeValue = jsonBiz.get("categories");
        String textValue = "";
        if (attributeValue!=null){
            //textValue = String.join(" ", (JSONArray)attributeValue);
            List<Object> categories = (JSONArray)attributeValue;
            for(Object category:categories) {
                doc.add(new TextField("categories", category.toString(), Field.Store.YES));
            }
        }
        // add geo location
        String latitude = jsonBiz.get("latitude").toString();
        String longitude = jsonBiz.get("longitude").toString();
        doc.add(new GeoPointField("geolocation", Double.valueOf(latitude), Double.valueOf(longitude), Field.Store.NO));
        return doc;
    }

    public static void main(String[] args) throws IOException, ParseException {
        String yelpDataDir = System.getenv("YELP_DATA_DIR");
        if (args != null && args.length==1){
            yelpDataDir = args[0];
        }

        if (yelpDataDir == null){
            System.err.println("Please specify the yelp data dir and run again:");
            System.err.println("java "+ YelpLuceneBusinessRAM.class.getSimpleName()+" <yelp data dir path>");
        }else{
            System.out.println("=============Started@"+ LocalDateTime.now()+"=============");
            System.out.println("Working with Yelp [business] data at: "+yelpDataDir);
            new YelpLuceneBusinessRAM().indexAndSearch(yelpDataDir, new ClassicSimilarity());
            System.out.println("============================================");
            System.out.println("Running same query again with a different similarity");
            System.out.println("============================================");
            new YelpLuceneBusinessRAM().indexAndSearch(yelpDataDir, new BM25Similarity());
            System.out.println("=============Completed@"+ LocalDateTime.now()+"=============");
        }
    }
}