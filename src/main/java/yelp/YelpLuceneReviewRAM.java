package yelp;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.ClassicSimilarity;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by wangw on 04-Apr-17.
 */
public class YelpLuceneReviewRAM extends YelpLuceneCommon {
    private static final String DEFAULT_SEARCH_FIELD = "text";
    private static final String[] QUERIES = {"pizza", "italian nice view", "+text:italian +stars:[5.0 TO 5.0]"};

    @Override
    public String getFileNameToIndex() {
        return "yelp_academic_dataset_review.json";
    }

    @Override
    public Document createDoc(JSONObject jsonData) {
        Document doc = new Document();
        for(String fieldName : (Set<String>) jsonData.keySet()) {
            Object fieldValue = jsonData.get(fieldName);
            switch(fieldName){
                case "text":
                    doc.add(new TextField(fieldName, fieldValue.toString(), Field.Store.YES));
                    break;
                case "date":
                    doc.add(new StringField(fieldName, fieldValue.toString(), Field.Store.YES));
                    break;
                case "business_id":
                    doc.add(new StringField(fieldName, fieldValue.toString(), Field.Store.YES));
                    break;
                case "user_id":
                    doc.add(new StringField(fieldName, fieldValue.toString(), Field.Store.YES));
                    break;
                case "review_id":
                    doc.add(new StringField(fieldName, fieldValue.toString(), Field.Store.YES));
                    break;
                case "stars":
                    doc.add(new DoublePoint(fieldName, Double.valueOf(fieldValue.toString())));
                    doc.add(new StoredField(fieldName, Double.valueOf(fieldValue.toString())));
                    break;
                case "useful":
                    doc.add(new LongPoint(fieldName, (Long)fieldValue));
                    doc.add(new StoredField(fieldName, (Long)fieldValue));
                    break;
                case "funny":
                    doc.add(new LongPoint(fieldName, (Long)fieldValue));
                    doc.add(new StoredField(fieldName, (Long)fieldValue));
                    break;
                case "cool":
                    doc.add(new LongPoint(fieldName, (Long)fieldValue));
                    doc.add(new StoredField(fieldName, (Long)fieldValue));
                    break;
            }
        }
        return doc;
    }

    @Override
    public List<Query> getQueries(Analyzer analyzer) throws ParseException {
        QueryParser queryParser = new YelpCustomQueryParser(DEFAULT_SEARCH_FIELD, analyzer);
        List<Query> result = new LinkedList<>();
        for(String queryText:QUERIES){
            result.add(queryParser.parse(queryText));
        }
        return result;
    }

    @Override
    public void printSearchResult(List<Document> results) {
        // 4. display results
        for(int i=0;i<results.size();++i) {
            Document d = results.get(i);
            System.out.println((i + 1) + ".\t" + d.get("date") + " | " + d.get("business_id") + " | " + d.get("user_id") + " | " + d.get("stars") +"\r\n"
                    + "\t" + d.get("text"));
        }
    }

    public static void main(String[] args) throws IOException, ParseException {
        String yelpDataDir = System.getenv("YELP_DATA_DIR");
        if (args != null && args.length==1){
            yelpDataDir = args[0];
        }

        if (yelpDataDir == null){
            System.err.println("Please specify the yelp data dir and run again:");
            System.err.println("java "+ YelpLuceneReviewRAM.class.getSimpleName()+" <yelp data dir path>");
        }else{
            System.out.println("=============Started@"+ LocalDateTime.now()+"=============");
            System.out.println("Working with Yelp [review] data at: "+yelpDataDir);
            new YelpLuceneReviewRAM().indexAndSearch(yelpDataDir, new ClassicSimilarity());
            System.out.println("============================================");
            System.out.println("Running same query again with a different similarity");
            System.out.println("============================================");
            new YelpLuceneReviewRAM().indexAndSearch(yelpDataDir, new BM25Similarity());
            System.out.println("=============Completed@"+ LocalDateTime.now()+"=============");
        }
    }
}
